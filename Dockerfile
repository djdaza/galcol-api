FROM node:14.17-alpine3.12 AS development

WORKDIR /opt/project/

COPY ./ ./
COPY ./*.json ./

RUN npm i -g @nestjs/cli
RUN npm i

CMD ["npm", "run", "start:dev"]
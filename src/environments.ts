import * as Joi from 'joi';

export const environments = {
    dev: '.env',
    stag: '.stag.env',
    prod: '.prod.env',
};

export const envSchema = Joi.object({
    JWT_SECRET: Joi.string().required(),

    POSTGRES_DB: Joi.string().required(),
    POSTGRES_USER: Joi.string().required(),
    POSTGRES_PASSWORD: Joi.string().required(),
    POSTGRES_HOST: Joi.string().required(),
    POSTGRES_PORT: Joi.number().required(),

    TYPEORM_CONNECTION: Joi.string().required(),
    TYPEORM_PORT: Joi.number().required(),
    TYPEORM_HOST: Joi.string().required(),
    TYPEORM_USERNAME: Joi.string().required(),
    TYPEORM_PASSWORD: Joi.string().required(),
    TYPEORM_DATABASE: Joi.string().required(),
    TYPEORM_SYNCHRONIZE: Joi.bool().required(),
    TYPEORM_LOGGING: Joi.bool().required(),
    TYPEORM_ENTITIES: Joi.string().required(),

    TYPEORM_MIGRATIONS: Joi.string().required(),
    TYPEORM_MIGRATIONS_DIR: Joi.string().required(),
    TYPEORM_MIGRATIONS_TABLE_NAME: Joi.string().required(),
});

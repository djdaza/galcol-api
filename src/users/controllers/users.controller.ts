import {
    Controller,
    Get,
    Param,
    Post,
    Body,
    Put,
    Delete,
    ParseIntPipe,
    UseGuards,
    HttpCode,
    HttpStatus,
} from '@nestjs/common';

import { UsersService } from '../services/users.service';
import { CreateUserDto, UpdateUserDto } from '../dtos/user.dto';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { Public } from 'src/auth/decorators/public.decorator';

@UseGuards(JwtAuthGuard)
@Controller('users')
export class UsersController {
    constructor(private usersService: UsersService) {}

    @Get()
    @Public()
    findAll() {
        return this.usersService.findUsers();
    }

    @Get(':id')
    @Public()
    get(@Param('id', ParseIntPipe) id: number) {
        return this.usersService.findUser(id);
    }

    @Post()
    @Public()
    @HttpCode(HttpStatus.CREATED)
    create(@Body() payload: CreateUserDto) {
        return this.usersService.createUser(payload);
    }

    @Put(':id')
    update(@Param('id', ParseIntPipe) id: number, @Body() payload: UpdateUserDto) {
        return this.usersService.updateUser(id, payload);
    }

    @Delete(':id')
    remove(@Param('id', ParseIntPipe) id: number) {
        return this.usersService.removeUser(id);
    }
}

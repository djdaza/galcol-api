import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import * as bcrypt from 'bcrypt';

import { User } from '../entities/users.entity';
import { CreateUserDto, UpdateUserDto } from '../dtos/user.dto';

@Injectable()
export class UsersService {
    constructor(@InjectRepository(User) private userRepo: Repository<User>) {}

    findUsers() {
        return this.userRepo.find();
    }

    async findUser(id: number) {
        const user = await this.userRepo.findOne(id);
        if (!user) {
            throw new NotFoundException(`User #${id} not found`);
        }
        return user;
    }

    async findByEmail(email: string) {
        const user = await this.userRepo.findOne({
            where: { email },
        });
        if (!user) {
            throw new NotFoundException(`User with email ${email} not found`);
        }
        return user;
    }

    async createUser(data: CreateUserDto) {
        const newUser = this.userRepo.create(data);
        const hashedPassword = await bcrypt.hash(newUser.password, 10);
        newUser.password = hashedPassword;
        return this.userRepo.save(newUser);
    }

    async updateUser(id: number, changes: UpdateUserDto) {
        const user = await this.findUser(id);
        this.userRepo.merge(user, changes);
        return this.userRepo.save(user);
    }

    removeUser(id: number) {
        return this.userRepo.delete(id);
    }
}

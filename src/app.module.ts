import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';

import config from './config';
import { AppService } from './app.service';
import { environments, envSchema } from './environments';
import { AppController } from './app.controller';
import { DatabaseModule } from './database/database.module';
import { UsersModule } from './users/users.module';
import { CoursesModule } from './courses/courses.module';
import { AuthModule } from './auth/auth.module';

@Module({
    imports: [
        ConfigModule.forRoot({
            envFilePath: environments[process.env.NODE_ENV] || '.env',
            load: [config],
            isGlobal: true,
            validationSchema: envSchema,
        }),
        DatabaseModule,
        UsersModule,
        CoursesModule,
        AuthModule,
    ],
    controllers: [AppController],
    providers: [AppService],
})
export class AppModule {}

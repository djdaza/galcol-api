import { Module } from '@nestjs/common';

import { CoursesService } from './services/courses/courses.service';
import { CoursesController } from './controllers/courses/courses.controller';
import { CategoriesService } from './services/categories/categories.service';
import { CategoriesController } from './controllers/categories/categories.controller';

@Module({
    controllers: [CategoriesController, CoursesController],
    providers: [CoursesService, CategoriesService],
})
export class CoursesModule {}
